<?php

declare(strict_types=1);

namespace Charm\Event;

use WeakReference;

trait EventEmitterTrait
{
    private array $handlers = [];

    /**
     * Override this method to augment event objects with additional
     * data when they are emitted.
     *
     * @param object $event
     * @return object
     */
    protected function extendEventData(EventInterface $event): void {
        return;
    }

    public function on(string $eventName, callable $handler, bool $useWeakReference = false): void
    {
        if ($useWeakReference) {
            $this->handlers[$eventName][] = WeakReference::create($handler);
        } else {
            $this->handlers[$eventName][] = $handler;
        }
    }

    public function off(string $eventName = null, callable $handler = null): void
    {
        if ($eventName === null) {
            $this->handlers = [];
            return;
        }
        if (null === $handler) {
            unset($this->handlers[$eventName]);
        } else {
            $this->handlers[$eventName] = array_filter($this->handlers[$eventName] ?? [], function ($handlerRef) use ($handler) {
                if (is_a($handlerRef, WeakReference::class)) {
                    $value = $handlerRef->get();
                    if ($value === null) {
                        return false;
                    }
                } else {
                    $value = $handlerRef;
                }
                return $handler !== $value;
            });
        }
    }

    /**
     * Emit an event
     *
     * @param string $eventName
     * @param EventInterface|object|array $data Arbitrary event data. 
     * @param boolean $cancelable Allow the event to be cancelled by setting the 'cancelled' property to true?
     * @return EventInterface
     */
    public function emit(string $eventName, mixed $event, bool $cancelable = true): EventInterface
    {
        if (!is_a($event, EventInterface::class)) {
            $event = new Event($eventName, $event, $cancelable);
        }

        $this->extendEventData($event);

        foreach ($this->handlers[$eventName] ?? [] as $index => $handlerRef) {
            if (is_a($handlerRef, WeakReference::class)) {
                $handler = $handlerRef->get();

                if ($handler === null) {
                    unset($this->handlers[$eventName][$index]);
                    continue;
                }
            } else {
                $handler = $handlerRef;
            }
            $handler($event, $eventName);
            if ($cancelable && $event->cancelled) {
                return $event;
            }
        }

        return $event;
    }

    public function removeAllListeners(): void {
        $this->handlers = [];
    }

    /**
     * Remove any weak-referenced event listeners that are gone.
     * 
     * @internal Used internally
     *
     * @return void
     */
    public function garbageCollectEventEmitters(): void {
        foreach ($this->handlers as $eventName => $handlers) {
            $this->handlers = array_filter($handlers, function ($handlerRef) {
                if (is_a($handlerRef, WeakReference::class)) {
                    $value = $handlerRef->get();
                    if ($value === null) {
                        return false;
                    }
                }
                return true;
            });
        }
    }
}
