<?php
declare(strict_types=1);

namespace Charm\Event;

interface EventEmitterInterface
{
    public function on(string $eventName, callable $handler): void;

    public function off(string $eventName = null, callable $handler = null): void;

    /**
     * Emit an event.
     *
     * @param EventInterface|object $event      arbitrary event data
     * @param bool                  $cancelable Allow the event to be cancelled by setting the 'cancelled' property to true?
     */
    public function emit(string $eventName, object $event, bool $cancelable): EventInterface;

    public function removeAllListeners(): void;
}
