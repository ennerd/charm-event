<?php
declare(strict_types=1);

namespace Charm\Event;

/**
 * @property string  $type
 * @property bool    $cancelable
 * @property bool    $cancelled
 * @property ?string $targetClass
 */
class Event implements EventInterface
{
    protected string $type;
    protected bool $cancelable;
    protected bool $cancelled = false;
    public ?string $targetClass = null;
    public object $data;

    /**
     * Create an event object.
     *
     * @param T $data
     */
    public function __construct(string $type, object $data, bool $cancelable = true)
    {
        $this->type = $type;
        $this->data = (object) $data;
        $this->cancelable = $cancelable;
    }

    public function &__get(string $key)
    {
        if (property_exists($this, $key)) {
            $val = $this->$key;

            return $val;
        }

        return $this->data->$key;
    }

    public function __set(string $key, mixed $value)
    {
        if ('cancelled' === $key && $this->cancelable) {
            $this->cancelled = (bool) $value;
        } elseif (property_exists($this, $key)) {
            throw new Error("Can't set the '$key' property on event objects");
        }
        $this->data->key = $value;
    }

    public function __isset(string $key)
    {
        return isset($this->data->$key);
    }

    public function __unset(string $key)
    {
        unset($this->data->$key);
    }

    public function cancel(): void
    {
        if (!$this->cancelable) {
            throw new Error('Event is not cancelable');
        }
        $this->cancelled = true;
    }

    /*
        public function __debugInfo() {
            $result = (array) $this->data + get_object_vars($this);
            return $result;
        }
    */
}
