<?php
declare(strict_types=1);

require('vendor/autoload.php');

class Example implements Charm\Event\EventEmitterInterface {
    use Charm\Event\EventEmitterTrait;
}

$instance = new Example();

// First handler
$instance->on('some-event', function($data) {
    echo "Got: ".$data->message."\n";
    $data->cancelled = true;
});
// Second handler won't run because the first handler set `$data->cancelled` to true
$instance->on('some-event', function($data) {
    echo "No message for me\n";
});

$instance->emit('some-event', $data = (object) [ 'message' => 'OK' ]);
